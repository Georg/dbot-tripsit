### TripSit related modules for dbot - revived

This houses forks of dbot modules specific to the TripSit IRC network.

The following issues have been resolved:

- Replaced alsuti uploader with a generic, self-hosted, https (curl) endpoint for dose log uploads

### Credits

All credits go to https://github.com/reality/ - Thank you for having made dbot and these modules possible. It served countless of users well over several years - and albeit bundled with the occasional complaints and bugs, it never left its place in many, many hearts.
