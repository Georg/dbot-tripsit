var _ = require('underscore');

var cap = function(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

var pages = function(dbot) {
    var pages = {
        '/factsheet/test': function(req, res) {
          res.end(req.user.access);
        },

        '/factsheet/wikitemplate/:drug': function(req, res) {
            this.api.getDrug(req.params.drug, function(drug) {
                if(drug) {
                    d = drug.properties;
if(!_.has(d,'categories')) d.categories = [];
res.header("Content-Type", "text/plain; charset=utf-8");
                    var template = this.config.wikiTemplate.format({
                        'dose': d.dose,
                        'onset': d.onset,
                        'duration': d.duration,
                        'avoid': d.avoid,
                        'categories': d.categories.join(', '),
                        'summary': d.summary,
                        'after-effects': d['after-effects'],
                        'effects': d.effects
                    });
                    template += '\n\n[[Category:Drugs]]\n';
                    _.each(d.categories,function(cat){template+='[[Category:'+cap(cat)+']]\n';});
                    res.end(template);
                }
            }.bind(this));
        },

        '/factsheet': function(req, res) {
            var drugs = []; 
            this.db.scan('drugs', function(drug) {
		if(drug && _.has(drug, 'name') && _.has(drug, 'properties')) {
		if(drug.name.length <= 4 || drug.name.indexOf('-') != -1) {
			drug.name = drug.name.toUpperCase();
		} else {
			drug.name = drug.name.charAt(0).toUpperCase() + drug.name.slice(1);
		}
drug.name = drug.name.replace(/MEO/, 'MeO');
drug.name = drug.name.replace(/ACO/, 'AcO');
drug.name = drug.name.replace(/NBOME/, 'NBOMe');
drug.name = drug.name.replace(/MIPT/, 'MiPT');
drug.name = drug.name.replace(/DIPT/, 'DiPT');
                drugs.push(drug); 
		}
            }, function() {
drugs = _.sortBy(drugs, 'name');
                res.render('fs_list', {
                    'drugs': drugs,
	            'thing': 'Factsheets'
                }); 
            });
        },

        '/factsheet/edit': function(req, res) {
            var name = req.body.name,
                params = req.body,
                pValues = _.omit(params, 'name', 'newname', 'categories', 'aliases', 'formatted_dose'),
                response = {
                    'error': null,
                    'drug': null
                };
	
		if(!_.has(req, 'user') || (_.has(req, 'user') && req.user.access == 'user')) {
		    response.error = 'Unauthenticated.';
		    return res.json(response);
		}

            this.api.getDrug(params.name, function(drug) {
                if(drug) {
                    if(_.has(params, 'categories')) {
                        var categories = params.categories.replace(/\s/g,'').split(',');
                            availableCategories = [ 'psychedelic', 'benzodiazepine',
                              'dissociative', 'opioid', 'depressant', 'common', 'stimulant', 'habit-forming', 'research-chemical', 'empathogen', 'deliriant', 'nootropic', 'tentative', 'inactive' ];
                        if(_.difference(categories, availableCategories).length !== 0) {
                            response.error = 'Invalid category.';
                            return res.json(response);
                        }
                    }
                    
                    drug.categories = categories;
                    
                    _.each(pValues, function(property, index) {
                        drug.properties[index] = property;      
                    });

                    this.db.save('drugs', drug.name, drug, function(err, drug) {
                        if(_.has(params, 'newname') && params.newname !== drug.name) {
                            this.api.getDrug(params.newname, function(oDrug) {
                                if(oDrug) {
                                    response.error = params.newname + ' already exists.';
                                    return res.json(response);
                                } else {
                                    this.api.delDrug(drug.name, function() {
                                        drug.name = params.newname;
                                        this.db.create('drugs', drug.name, drug, function() {
                                            this.api.getDrug(drug.name, function(drug) {
                                                response.drug = drug;
                                                res.json(response);
                                                dbot.say('tripsit', '#content', dbot.t('websetdrug_log', {
                                                    'user': req.user.primaryNick,
                                                    'drug': drug.name
                                                }));
                                            });
                                        }.bind(this));
                                    }.bind(this));
                                }
                            }.bind(this));
                        } else {
                            this.api.getDrug(drug.name, function(drug) {
                                response.drug = drug;
                                res.json(response);
                                dbot.say('tripsit', '#content', dbot.t('websetdrug_log', {
                                    'user': req.user.primaryNick,
                                    'drug': drug.name
                                }));

                            });
                        }
                    }.bind(this));
                } else {
                    response.error = 'Unknown drug.';
                    res.json(response);
                }
            }.bind(this));
        },

        '/factsheet/:drug': function(req, res) {
            this.api.getDrug(req.params.drug, function(drug) {
                if(drug && !_.has(drug, 'err')) {
if(drug.name.length <= 3 || drug.name.indexOf('-') != -1) {
			drug.name = drug.name.toUpperCase();
		} else {
			drug.name = drug.name.charAt(0).toUpperCase() + drug.name.slice(1);
		}
                    if(_.has(drug.properties, 'dose')) { // LAAAAME
                        drug.properties.dose = drug.properties.dose.split('|');
                    }

                    if(_.has(drug.properties, 'onset')) { // LAAAAME
                        drug.properties.onset = drug.properties.onset.split('|');
                    }

                    if(_.has(drug.properties, 'wiki')) { // This is also kinda lame
                        drug.wiki = drug.properties.wiki;
                        delete drug.properties.wiki;
                    }

			var order = _.union(['summary', 'dose', 'onset', 'duration'], _.keys(drug.properties));

                    res.render('factsheet', {
                        'drug': drug,
			'order': order,
			'thing': drug.name + ' Factsheet'
                    });
                } else {
                    res.render('error', {
                        'name': req.params.drug
                    });
                }
            });
        },

        '/chanlist': function(req, res) {
            res.render('oclist', {
                 'channels': this.channels
            });
        }
    };

    pages['/factsheet/edit'].type = 'post';

    return pages;
}

exports.fetch = function(dbot) {
    return pages(dbot);
};
