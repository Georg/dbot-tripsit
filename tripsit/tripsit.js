/**
 * Module Name: TripSit
 * Description: Functionality specific to the TripSit project over at
 * http://tripsit.me/
 */
var async = require('async'),
    _ = require('underscore')._,
    moment = require('moment'),
    fs = require('fs'),
    request = require('request');

function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

var tripsit = function(dbot) {
    if(!_.has(dbot.db, 'tNotifies')) dbot.db.tNotifies = [];
    this.tNotifies = dbot.db.tNotifies;
    this.tcNotifies = dbot.db.tcNotifies;
    this.ndb = dbot.modules.report.db;
    this.tsChecks = {};
    this.channels = [];
    this.recentNotifies = [];
    this.combos = JSON.parse(fs.readFileSync('combo_beta.json', 'utf-8'));
    this.opiates = JSON.parse(fs.readFileSync('oconvert.json', 'utf-8'));
    this.bTable = { // TODO: Put in config
        'xanax': 0.5,
        'alprazolam': 0.5,
        'bromazepam': 5.5,
        'clonazepam': 0.5,
        'klonopin': 0.5,
        'lexotan': 5.5,
        'lexamil': 5.5,
        'chlordazepoxide': 25,
        'librium': 25,
        'etizolam': 1,
        'clobazam': 20,
        'valium': 10,
        "clorazepate": 15,
        "diazepam": 10,
        "estazolam": 1.5, 
        "proSom": 1.5, 
        "nuctalon": 1.5, 
        "flunitrazepam": 1,
        "rohypnol": 1,
        "flurazepam": 23,
        "dalmane": 23,
        "halazepam": 20,
        "paxipam": 20,
        "ketazolam": 23,
        "paxipam": 23,
        "loprazolam": 1.5, 
        "dormonoct": 1.5, 
        "lorazepam": 1,
        "ativan": 1,
        "lormetazepam": 1.5, 
        "noctamid": 1.5, 
        "medazepam": 10,
        "nobrium": 10,
        "nitrazepam": 10,
        "mogadon": 10,
        "nordazepam": 10,
        "oxazepam": 20,
        "serax": 20,
        "prazepam": 15,
        "centrax": 15,
        "quazepam": 20,
        "Doral": 20,
        "temazepam": 20,
        "Restoril": 20,
        "triazolam": 0.5, 
        "halcion": 0.5
    };

    var commands = {
    '~wat': function(event){var e=event.params.splice(1,event.params.length-1);var s=(e.length/_.random(2,4));for(var i=0;i<s;i++){e[_.random(0,e.length)]='wat';}event.reply(e.join(' '));},
    '~settopic': function(event) {
 if(event.channel == '#uk') { event.instance.connections.tripsit.send('TOPIC #uk :' + event.params.splice(1, event.params.length-1).join(' ').trim()); }
 },
        '~teamtripsit': function(event) {
            var channel = event.channel;
            var nicks = _.chain(channel.nicks)
                         .filter(function(user) { 
                             if(!_.include(['bot', 'DrTripServington', 'tripbot'], user.name)) {
                                 if(channel.name !== '#tripsitvip') {
                                     return user.op || user.voice; 
                                 } else {
                                     return user.op;
                                 }
                              }
                         })
                         .pluck('name')
                         .value();
            event.reply('ATTENTION COCONUT-MASTERS: ' + nicks.join(', '));
        },

        '~teammeth': function(event) {
            var channel = event.channel;
            var nicks = _.chain(channel.nicks)
                         .filter(function(user) { 
                             if(!_.include(['bot', 'DrTripServington', 'tripbot'], user.name)) {
                                 if(channel.name !== '#tripsitvip') {
                                     return user.op || user.voice; 
                                 } else {
                                     return user.op;
                                 }
                              }
                         })
                         .pluck('name')
                         .value();
            event.reply('METH TEAM ACTIVATE: ' + nicks.join(', '));
        },

    '~pretzels': function(event) {
        event.reply(_.map('these pretzels are making me thirsty', function(x) { if(_.random(2) == 1) { x = x.toUpperCase(); } return x; }).join(''));    
        },

        '~highfive': function(event) { 
            if(event.params[1]) { 
                dbot.api.quotes.getQuote('highfives', function(quote) {
                    if(!quote) quote = 'shatters mountains';
                    event.reply(event.user + ' and ' + event.params[1] + ' highfive so hard it ' + quote);
                });
            }
        },

        '~tripsit': function(event) {
            var channel = event.channel.name,
                helpUser = event.params[1] || event.user;

            if(event.params[1]) {
              dbot.api.users.resolveUser(event.server, helpUser, function(err, user) {
                if(!err && user) {
                  dbot.api.event.emit('new_tripsit_user', [ user ]);
                  this.tsChecks[user.id] = setTimeout(function() {
                    dbot.api.event.emit('tripsit_no_notify', [ user ]);
                    delete this.tsChecks[user.id];
                  }.bind(this), 600000);
                }
              }.bind(this));
            } else {
              dbot.api.event.emit('new_tripsit_user', [ event.rUser ]);
              this.tsChecks[event.rUser.id] = setTimeout(function() {
                dbot.api.event.emit('tripsit_no_notify', [ event.rUser ]);
                delete this.tsChecks[event.rUser.id];
              }.bind(this), 600000);
            }

            if(channel === '#tripsit1' || channel === '#tripsit2' || channel == '#tripsit3' || channel === '#tripsit' || channel == '#letstalk' || channel == '#feelingblue') {
                dbot.api.report.notify('tripsit', event.server, event.rUser, 
                        channel, dbot.t('tripsit_help', { 'user': helpUser, 'channel': channel }));
                dbot.say(event.server, '#teamtripsit', dbot.t('tripsit_help', 
                    { 'user': helpUser, 'channel': channel }));
                dbot.say(event.server, '#tripsitvip', dbot.t('tripsit_help', 
                    { 'user': helpUser, 'channel': channel }));

                _.each(dbot.db.tcNotifies, function(uid) {
                  dbot.api.users.getUser(uid, function(err, user) {
                    if(!err && user) {
                      dbot.say(event.server, user.currentNick, dbot.t('tripsit_help', 
                        { 'user': helpUser, 'channel': channel }));
                    }
                  });
                });

                event.reply(dbot.t('tripsit_help_confirm', { 'user': helpUser }));
            }
        },

	'~wiki': function(event) {
            request.get('http://wiki.tripsit.me/api.php', {
                'qs': {
			'action': 'opensearch',
			'search': event.input[1],
			'limit': 1,
			'namespace': 0,
			'format': 'json'
                },
                'json': true
            }, function(err, res, body) {

            console.log(body);
		if(!err && body[1].length !== 0) {
			event.reply(event.input[1] + ': https://wiki.tripsit.me/wiki/'+body[1][0].replace(/\s/g, '_'));
		} else {
			event.reply(event.input[1] + ' not found.');
		}
	    });
	},

        '~nstats': function(event) {
            var start = moment(event.input[1]),
                end = moment(event.input[2]),
                feedback = {},
                tagBacks = {};

            if(start == NaN || end == NaN) return event.reply('Invalid dates innit.');
            
            this.ndb.scan('notifies', function(notify) {
                notify.time = moment(notify.time);
                if(notify.time > start && notify.time < end) {
                    if(!_.has(feedback, notify.user)) {
                        feedback[notify.user] = {
                            'id': notify.user,
                            'count': 0
                        };
                    }
                    feedback[notify.user].count++;

                    if(notify.message) {
                        _.each(notify.message.match(/ @([\d\w*|-]+)/g), function(tag) {
                            tag = tag.substr(2);
                            if(!_.has(tagBacks, tag)) {
                                tagBacks[tag] = 0;
                            }
                            tagBacks[tag]++;
                        });
                    }
                }
            }, function() {
               async.each(_.keys(feedback), function(f, next) {
                    f = feedback[f];
                    dbot.api.users.getUser(f.id, function(err, user) {
            if(user) {                        
                f.nick = user.primaryNick;
            } else {
                f.nick = f.id;
            }
                        next();
                    });
                }, function() {
                    async.each(_.keys(tagBacks), function(t, next) {
                        console.log("'"+t+"'");
                        dbot.api.users.resolveUser(event.server, t, function(err, user) {
                            if(user) {
                            console.log(user.id + ': ' + t);
                                if(!_.has(feedback, user.id)) feedback[user.id] = {
                                    'id': user.id,
                                    'nick': user.primaryNick,
                                    'count': 0
                                }
                                feedback[user.id].count += tagBacks[t];
                            }
                            next();
                        }, true);
                    }, function() {
                        var total = 0;
                        async.eachSeries(_.keys(feedback), function(f, next) {
                          var f = feedback[f];
                          dbot.api.nickserv.getUserHost(event.server, f.nick, function(h) {
                            if(h) {
                              var pos = h.match(/tripsit\/(.+)\/.+/);
                            } else {
                              pos == null;
                            }
                              if(pos && pos[1] != 'user') {
                                pos = pos[1]; 
                              } else {
                                pos = 'nonstaff';
                              }
                              

                              event.reply(f.nick + ' - ' + pos + ': ' + f.count);
                              //event.reply(f.nick + ': ' + f.count);
                              total += f.count;
                              setTimeout(function() {
                                  next(); 
                              }, 200);
                           });
                        }, function() {
                            event.reply('Total: ' + total); 
                        });
                    });
                });
            });
        },

        '~gettripsitcalls': function(event) {
          if(!_.include(this.tcNotifies, event.rUser.id)) {
            this.tcNotifies.push(event.rUser.id);
            event.reply("You will now be notified of ~tripsit calls");
          } else {
            event.reply("You were already subscribed to ~tripsit calls!");
          }
        },

        '~notripsitcalls': function(event) {
          if(_.include(this.tcNotifies, event.rUser.id)) {
            dbot.db.tcNotifies = _.without(this.tcNotifies, event.rUser.id);
            this.tcNotifies = dbot.db.tcNotifies;
            event.reply("You are no longer subscribed to ~tripsit calls");
          } else {
            event.reply("You weren't subscribed to ~tripsit calls in the first place mate");
          }
        },

        '~gettripsitentries': function(event) {
            if(!_.include(this.tNotifies, event.rUser.id)) {
                this.tNotifies.push(event.rUser.id);
                event.reply(dbot.t('tripsit_notifies_on'));
            } else {
                event.reply(dbot.t('tripsit_already_notified'));
            }
        },

        '~notripsitentries': function(event) {
            if(_.include(this.tNotifies, event.rUser.id)) {
                // ...
                dbot.db.tNotifies = _.without(this.tNotifies, event.rUser.id);
                this.tNotifies = dbot.db.tNotifies;
                event.reply(dbot.t('tripsit_notifies_off'));
            } else {
                event.reply(dbot.t('tripsit_notifies_already_off'));
            }
        },

    '~disarm': function(event) {
        if(event.params[1]) {
            dbot.api.users.resolveUser(event.server, event.params[1], function(err, user) {
                if(user && _.has(this.tsChecks, user.id)) {
                        clearTimeout(this.tsChecks[user.id]);
                        delete this.tsChecks[user.id];
                    event.reply('Disarmed ' + user.currentNick);
                } else {
                    event.reply('No check for that user');
                }
            }.bind(this));
        }
    },

        '~autodrugcategories': function(event) {
            this.db.scan('drugs', function(drug) {
                var summary = drug.properties.summary || '';
                if(summary.match(/psychedelic/i)) drug.categories.push('psychedelic');
                if(summary.match(/dissociative/i)) drug.categories.push('dissociative');
                if(summary.match(/opiate/i)) drug.categories.push('opioid');
                if(summary.match(/opioid/i)) drug.categories.push('opioid');
                if(summary.match(/deliriant/i)) drug.categories.push('deliriant');
                if(summary.match(/stimulant/i)) drug.categories.push('stimulant');
                if(summary.match(/depressant/i)) drug.categories.push('depressant');
                if(summary.match(/benzo/i)) drug.categories.push('benzodiazepine');
                drug.categories = _.uniq(drug.categories);
                if(drug.categories.length != 0) {
                    this.db.save('drugs', drug.name, drug, function(){});
                }
            }.bind(this), function() {});
        },

        '~drug': function(event) {
            var dName = event.input[1],
                property = event.input[2];

            this.api.getIRCFormattedDrug(dName, property, function(response) {
              if(response) {
                event.reply(response);
              } else {
                event.reply('No information for ' + dName);
              }
            });
         },

        '~factsheet': function(event) {
            var dName = event.params[1].toLowerCase();
            this.api.getDrug(dName, function(drug) {
                if(drug) {
                    event.reply(dName + ': http://drugs.tripsit.me/'+drug.name);
                } else {
                    event.reply('No data about ' + dName);
                }
            });
        },

        '~setcat': function(event) {
            var cName = event.input[1],
                property = event.input[2].toLowerCase(),
                content = event.input[3].trim();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrugCategory(cName, function(category) {
                    if(property == 'tips') return event.reply('Use ~addcattip');

                    if(category) {
                      this.api.setCategoryProperty(category, property, content, function() {
                          event.reply('Set property ' + property + ' of ' + cName);
                          dbot.say(event.server, '#content', dbot.t('setdrug_log', {
                              'user': event.user,
                              'property': property,
                              'drug': cName,
                              'content': content
                          }));
                      });
                    } else {
                      event.reply('No such category.');
                    }
                }.bind(this));
            }
        },

        '~rmdrugsource': function(event) {
          var dName = event.input[1],
                property = event.input[2],
                content = event.input[3].trim();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
              this.api.getDrug(dName, function(drug) {
                  if(drug && !_.has(drug, 'err')) {
                    if(!_.has(drug, 'sources')) {
                      return event.reply('Drug has no sources anyway!');
                    }
                    if(property == 'general') {
                      property = '_general';
                    }
                    property = property.toLowerCase();

                    if(_.has(drug.sources, property)) {
                      if(_.include(drug.sources[property], content)) {
                        drug.sources[property] = _.without(drug.sources[property], content);

                        if(property == '_general') {
                          property = 'general information'
                        }

                        this.db.save('drugs', drug.name, drug, function(err) {
                          event.reply('Removed source for ' + property + ' of ' + dName);
                          dbot.say(event.server, '#content', dbot.t('rmsource_log', {
                              'user': event.user,
                              'property': property,
                              'drug': dName,
                              'content': content
                          }));
                        });
                      } else {
                        event.reply('Source not found.');
                      }
                    } else {
                      event.reply('No sources for that property!');
                    }
                  } else {
                    event.reply('No such drug!');
                  }
              }.bind(this));
            }
        },

        '~adddrugsource': function(event) {
          var dName = event.input[1],
                property = event.input[2],
                content = event.input[3].trim();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrug(dName, function(drug) {
                    if(drug && !_.has(drug, 'err')) {
                      if(!_.has(drug, 'sources')) {
                        drug.sources = {};
                      }
                      property = property.toLowerCase();
                      if(property == 'general') {
                        property = '_general';
                      }

                      if(_.has(drug.properties, property) || property == '_general') {
                        if(!_.has(drug.sources, property)) {
                          drug.sources[property] = [];
                        }
                        if(_.include(drug.sources[property], content)) {
                          return event.reply('That source already exists!');
                        }
                        drug.sources[property].push(content);
                      } else {
                        return event.reply('No such property, perhaps you meant to add a general source?');
                      }

                      if(property == '_general') {
                        property = 'general information'
                      }

                      this.db.save('drugs', drug.name, drug, function(err) {
                        event.reply('Added source for ' + property + ' of ' + dName);
                        dbot.say(event.server, '#content', dbot.t('addsource_log', {
                            'user': event.user,
                            'property': property,
                            'drug': dName,
                            'content': content
                        }));
                      });
                    } else {
                      event.reply('No such drug!');
                    }
                }.bind(this));
            }
        },

        '~setdrug': function(event) {
            var dName = event.input[1],
                property = event.input[2].toLowerCase(),
                content = event.input[3].trim();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
              if(property == '_general') {
                return event.reply('Reserved property name, please choose another');
              }
                this.api.getDrug(dName, function(drug) {
                        if(property == 'aliases') return event.reply('Use ~setdrugalias');
                        if(property == 'categories') return event.reply('Use ~addrugcategory or ~rmdrugcategory');
                    if(drug && !_.has(drug, 'err')) {
                        this.api.setDrugProperty(drug, property, content, function() {
                            event.reply('Set property ' + property + ' of ' + dName);
                            dbot.say(event.server, '#content', dbot.t('setdrug_log', {
                                'user': event.user,
                                'property': property,
                                'drug': dName,
                                'content': content
                            }));
                        });
                    } else {
                        this.api.createDrug(dName, function() {
                            this.commands['~setdrug'](event);
                        }.bind(this));
                    }
                }.bind(this));
            }
        },

	'~drugs': function(event) {
	    this.commands['~drug'](event);
        },

        '~setdrugprettyname': function(event) {
            var dName = event.input[1].toLowerCase(),
                aName = event.input[2];

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                    this.api.getDrug(dName, function(drug) {
                        if(drug) {
                                drug.pretty_name = aName;
                                this.db.save('drugs', drug.name, drug, function() {});
                                event.reply(aName + ' is now they pretty name of ' + dName);
                                dbot.say(event.server, '#content', dbot.t('setdrug_log', {
                                    'user': event.user,
                                    'property': 'pretty name',
                                    'drug': dName,
                                    'content': aName 
                                }));
                        } else {
                            event.reply('No such drug.');
                        }
                    }.bind(this));
            }
        },


        '~setdrugcategory': function(event) {
            var dName = event.input[1].toLowerCase(),
                aName = event.input[2].toLowerCase();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                var availableCategories = [ 'psychedelic', 'benzodiazepine',
                    'dissociative', 'opioid', 'depressant', 'common', 'barbiturate', 'supplement', 'stimulant', 'habit-forming', 'research-chemical', 'empathogen', 'deliriant', 'nootropic', 'tentative', 'ssri', 'maoi', 'inactive' ];
                if(_.include(availableCategories, aName)) {
                    this.api.getDrug(dName, function(drug) {
                        if(drug) {
                            if(!_.include(drug.categories, aName)) {
                                if(!_.has(drug, 'categories')) {
                                    drug.categories = [];        
                                }
                                drug.categories.push(aName);
                                this.db.save('drugs', drug.name, drug, function() {});
                                event.reply(aName + ' is now an category of ' + dName);
                                dbot.say(event.server, '#content', dbot.t('setdrug_log', {
                                    'user': event.user,
                                    'property': 'category',
                                    'drug': dName,
                                    'content': aName 
                                }));
                            } else {
                                event.reply(aName + ' is already a category of ' + dName);
                            }
                        } else {
                            event.reply('No such drug.');
                        }
                    }.bind(this));
                } else {
                    event.reply('Nah mate, available drug categories are: ' +
                        availableCategories.join(', '));
                }
            }
        },

        '~rmdrugcategory': function(event) {
            var dName = event.input[1].toLowerCase(),
                aName = event.input[2].toLowerCase();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrug(dName, function(drug) {
                    if(drug) {
                        if(_.include(drug.categories, aName)) {
                            drug.categories = _.without(drug.categories, aName);
                            this.db.save('drugs', drug.name, drug, function() {});
                            event.reply(aName + ' is no longer a category of ' + dName);
                            dbot.say(event.server, '#content', dbot.t('rmdrugproperty_log', {
                                    'user': event.user,
                                    'property': 'category ' + aName,
                                    'drug': dName,
                                    'content': aName 
                                }));

                        } else {
                            event.reply(aName + ' isn\'t category of ' + dName);
                        }
                    } else {
                        event.reply('No such drug.');
                    }
                }.bind(this));
            }
        },

	'~rmdrugalias': function(event) {
            var dName = event.input[1].toLowerCase(),
                aName = event.input[2].toLowerCase();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrug(dName, function(drug) {
                    if(drug) {
                        if(_.include(drug.aliases, aName)) {
                            drug.aliases = _.without(drug.aliases, aName);
                            this.db.save('drugs', drug.name, drug, function() {});
                            event.reply(aName + ' is no longer an alias of ' + dName);
                            dbot.say(event.server, '#content', dbot.t('rmdrugproperty_log', {
                                    'user': event.user,
                                    'property': 'alias ' + aName,
                                    'drug': dName,
                                    'content': aName 
                                }));

                        } else {
                            event.reply(aName + ' isn\'t an alias of ' + dName);
                        }
                    } else {
                        event.reply('No such drug.');
                    }
                }.bind(this));
            }
        },

        '~addcattip': function(event) {
          var cat = event.input[1],
              tip = event.input[2];

          this.api.getDrugCategory(cat, function(category) {
            if(category) {
              category.tips.push(tip);
              this.db.save('drug_categories', category.name, category, function() {
                event.reply('Added tip to ' + cat);

                dbot.say(event.server, '#content', dbot.t('setdrug_log', {
                      'user': event.user,
                      'property': 'tip',
                      'drug': cat,
                      'content': tip
                  }));
              });
            } else {
              event.reply('No such drug category.');
            }
          }.bind(this));
        },

        '~rmcattip': function(event) {
          var cat = event.input[1],
              tip = event.input[2];

          this.api.getDrugCategory(cat, function(category) {
            if(category) {
              var len = category.tips.length;
              category.tips = _.without(category.tips, tip);

              if(len != category.tips.length) {
                this.db.save('drug_categories', category.name, category, function() {
                  event.reply('Removed tip from ' + cat);

                  dbot.say(event.server, '#content', dbot.t('rmdrugproperty_log', {
                        'user': event.user,
                        'property': 'tip',
                        'drug': cat,
                    }));
                });
              } else {
                event.reply('Tip not found');
              }
            } else {
              event.reply('No such drug category.');
            }
          }.bind(this));
        },

        '~setdrugalias': function(event) {
            var dName = event.input[1].toLowerCase(),
                aName = event.input[2].toLowerCase();

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrug(dName, function(drug) {
                    if(drug && !_.has(drug, 'err')) {
                        this.api.getDrug(aName, function(aDrug) {
                            if(!aDrug || _.has(aDrug, 'err')) {
                                if(!_.has(drug, 'aliases')) {
                                    drug.aliases = [];        
                                }
                                drug.aliases.push(aName);
                                this.db.save('drugs', drug.name, drug, function() {});
                                event.reply(aName + ' is now an alias of ' + dName);
                            } else {
                                event.reply(aName + ' is already recorded as a drug.');
                            }
                        }.bind(this));
                    } else {
                        event.reply('No such drug.');
                    }
                }.bind(this));
            }
        },

        '~setdrugaliasparent': function(event) {
            var dName = event.params[1].toLowerCase();

            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);
            
            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrug(dName, function(drug) {
                    if(drug) {
                        if(_.include(drug.aliases, dName)) {
                            this.api.delDrug(drug.name, function() {
                                drug.aliases.push(drug.name);
                                drug.name = dName;
                                drug.aliases = _.without(drug.aliases, dName);
                                this.db.save('drugs', dName, drug, function() {
                                    event.reply(dName + ' is now parent.');
                                });
                            }.bind(this));
                        } else {
                            event.reply('Not even an alias.');
                        }
                    } else {
                        event.reply('No such drug.');
                    }
                }.bind(this));
            }
        },

        '~rmdrug': function(event) {
            var dName = event.params[1],
                pName = event.params[2];

            // I'm sorry Jesus
            var allowedUsers = _.union(dbot.config.admins,
                dbot.config.moderators, dbot.config.power_users,
                this.config.tripsitters);

            if(_.include(allowedUsers, event.rUser.primaryNick)) {
                this.api.getDrug(dName, function(drug) {
                    if(drug) {
                        if(_.isUndefined(pName)) {
                            this.api.delDrug(dName, function() {
                                event.reply('Removed ' + dName + ' from drugs.');
                                dbot.say(event.server, '#content', dbot.t('rmdrug_log', {
                                    'user': event.user,
                                    'drug': dName
                                }));
                            });
                        } else {
                            pName = pName.trim();
                            if(_.has(drug.properties, pName)) {
                                this.api.delDrugProperty(drug, pName, function() {
                                    event.reply(pName + ' removed from ' + dName);
                                    dbot.say(event.server, '#content', dbot.t('rmdrugproperty_log', {
                                        'user': event.user,
                                        'drug': dName,
                                        'property': pName
                                    }));
                                });
                            } else {
                                event.reply('No such property in ' + drug.name);
                            }
                        }
                    } else {
                        event.reply('No such drug.');
                    }
                }.bind(this));
            }
        },

        '~bdiaz': function(event) {
            var benzo = event.params[1].toLowerCase(),
                amount = event.params[2].replace('mg', '');

            if(_.has(this.bTable, benzo)) {
                var diazepam = (amount / this.bTable[benzo]) * 10;
                event.reply(event.params[2] + ' ' + benzo + ' ~= ' + diazepam + 'mg diazepam.');
            } else {
                event.reply(event.params[1] + ' not found.');
            }
        },

        '~bconvert': function(event) {
            var sAmount = event.params[1].replace('mg', ''),
                bSource = event.params[2],
                bTarget = event.params[3];

            if(_.has(this.bTable, bSource) && _.has(this.bTable, bTarget)) {
                var sDiaz = (sAmount / this.bTable[bSource]) * 10,
                    tDiaz = (this.bTable[bTarget] / this.bTable[bTarget]) * 10,
                    conv = (sDiaz / tDiaz) * this.bTable[bTarget];
                event.reply(sAmount + 'mg ' + bSource + ' ~= ' + conv + 'mg ' + bTarget);
            } else {
                event.reply('Unknown benzos.');
            }
        },

        '~staffstatus': function(event) {
          var target = (event.params[1] || event.user).trim();
          dbot.api.users.resolveUser(event.server, target, function(err, user) {
            if(!err && user) {
              dbot.api.nickserv.getUserHost(event.server, user.primaryNick, function(host) {
                if(host) {
                  var position = host.split('/')[1];
                  if(position) {
                    event.reply(user.primaryNick + ' is ' + position);

                    if(position != 'user') {
                      dbot.api.sstats.getUserStats(user.id, function(uStats) {
                        if(uStats && uStats.last) {
                          event.reply(user.primaryNick + ' was last active in a public channel ' + moment(uStats.last).fromNow()); 
                        }
                        var lastTripsitNotify = null,
                            lastTripsitTime = 0,
                            lastModNotify = null,
                            lastModTime = 0;

                        dbot.modules.report.db.search('notifies', {
                          'server': event.server
                        }, function(notify) {
                          if(notify.channel == '#tripsit' && notify.message.match(user.primaryNick) && notify.time > lastTripsitTime) {
                            lastTripsitNotify = notify;
                            lastTripsitTime = notify.time;
                          }
                          if((notify.type == 'quiet' || notify.type == 'ban' || notify.type == 'warn') && notify.user == user.id && notify.time > lastModTime) {
                            lastModNotify = notify;
                            lastModTime = notify.time;
                          }
                        }, function() {
                          if(lastTripsitTime != 0) {
                            event.reply(user.primaryNick + ' was last tagged in a #tripsit notify ' + moment(lastTripsitTime).fromNow());
                          } else {
                            event.reply('Can\'t find the last time the ' + user.primaryNick + ' was tagged in a #tripsit notify.');
                          }
                          if(lastModTime != 0) {
                            var what;
                            if(lastModNotify.type == 'quiet') {
                              what = 'quieted ' + lastModNotify.message.match(/quieted ([^ \.]+)/)[1] + ' in ' + lastModNotify.channel;
                            } else if(lastModNotify.type == 'warn') {
                              what = 'warned ' + lastModNotify.message.match(/warning to ([^ \.]+)/)[1];
                            } else if(lastModNotify.type == 'ban') {
                              if(lastModNotify.message.match(/AKilled/)) {
                                what = 'special-k-lined ' + lastModNotify.message.split(' ')[0];
                              } else {
                                what = 'banned ' + lastModNotify.message.match(/banned ([^ ]+)/)[1];
                              }
                            }

                            event.reply(user.primaryNick + ' last performed a mod action ' + moment(lastModTime).fromNow() + ' ('+what+')');
                          } else if(position != 'tripsitter') {
                            event.reply('Can\'t find the last time the ' + user.primaryNick + ' performed a mod action.');
                          }
                        });
                      });
                    }
                  } else {
                    event.reply(user.primaryNick + ' does not seem to be registered!');
                  }
                } else {
                  event.reply('Can\'t find a hostmask for ' + user.primaryNick);
                }
              });
            } else {
              event.reply('no idea who that is m8.');
            }
          });
        },
        
        '~oconvert': function(event) {
            var sAmount = event.params[1].replace('mg', ''),
                sRoa = event.params[2].toLowerCase(),
                oSource = event.params[3].toLowerCase(),
                tRoa = event.params[4].toLowerCase(),
          	oTarget = event.params[5].toLowerCase();

            if(_.has(this.opiates, oSource) && _.has(this.opiates, oTarget) && _.has(this.opiates[oTarget].ba, tRoa)) {
                // Work out the amount of morphine is equivalent to sAmount of oSource

		if(sRoa == 'iv') {
			var sMorphRoa = (sAmount / this.opiates[oSource].toMorphine) * 10;
		} else {
			var sMorphRoa = (this.opiates[oSource].ba[sRoa]/10)*sAmount/10;
		}
                //var sMorphRoa = 10*sMorph/(this.opiates[oSource].ba[sRoa]/10);

                // Work out the amount of oTarget is equivalent to that amount of morphine at 100%
                var tMorph = (this.opiates[oTarget].toMorphine / this.opiates[oTarget].toMorphine) * 10;

                var conv = (sMorphRoa / tMorph) * this.opiates[oTarget].toMorphine;


                // Work out the amount of oTarget that is equivalent to that amount of morphine at x%
                    // 10m/0.9 = r
                event.reply(sAmount + 'mg ' + sRoa + ' ' + oSource + ' ~= ' + (10*conv/(this.opiates[oTarget].ba[tRoa]/10)).toFixed(2) + 'mg ' + tRoa + ' ' + oTarget + 
                    ' [BETA][Note: Equianalgesic conversions are given as a guideline and cannot be entirely trusted. Effects, onset etc may differ between drugs and ROA.]');


            } else {
                event.reply('Unknown opiates or roas.');
            }
        },
        
        '~combo': function(event) {
          var d1 = event.input[1].toLowerCase(),
              d2 = event.input[2].toLowerCase();

          this.api.getInteraction(d1, d2, function(res) {
            if(res && !_.has(res, 'err')) {
              var output = res.interactionCategoryA + ' + ' + res.interactionCategoryB + ': ' + res.status;
              if(_.has(res, 'note')) { 
                output += '. ' + res.note;
              }
              event.reply(output);
            } else {
              if(res.code == 'ssb') {
                event.reply('Combination of benzos may unpredictably increase the intensity of undesirable effects.');
              } else if(res.code == 'ssc') {
                event.reply('Drugs are of the same safety category.');
              } else {
                event.reply('Unknown combo (that doesn\'t mean it\'s safe). Known combos: ' +
                    _.keys(this.combos).join(', ') + '.');
              }
            }
          }.bind(this));
        },

        '~volume': function(event) {
            var total = event.input[1],
                perml = event.input[2];

            event.reply('For a solution with ' + perml + 'mg/ml, add ' + total + 'mg to ' + (total/perml) + 'ml.');
        },

        '~perml': function(event) {
            var material = event.input[1],
                liquid = event.input[2];

            event.reply('If you put ' + material + 'mg in ' + liquid + 'ml of solution, it will be ' + (material / liquid) + 'mg/ml.');
        }
    };
    this.commands = commands;
    this.commands['~nstats'].regex = [/^nstats ([\d\w\-]+[\d\w\s\-]*)[ ]?=[ ]?(.+)$/, 3];
    this.commands['~drug'].regex = /^drug ([^ ]+) ?(.+)?$/;
    this.commands['~setdrugalias'].regex = /^setdrugalias ([^ ]+) ([^ ]+)/;
    this.commands['~combo'].regex = /^combo ([^ ]+) ([^ ]+)/;
    this.commands['~volume'].regex = /^volume ([^ ]+) ([^ ]+)/;
    this.commands['~perml'].regex = /^perml ([^ ]+) ([^ ]+)/;
    this.commands['~wiki'].regex = [/^wiki (.+)/, 2];
    this.commands['~setdrugcategory'].regex = /^setdrugcategory ([^ ]+) ([^ ]+)/;
    this.commands['~addcattip'].regex = /^addcattip ([^ ]+) (.+)/;
    this.commands['~rmcattip'].regex = /^rmcattip ([^ ]+) (.+)/;
    this.commands['~setdrugprettyname'].regex = /^setdrugprettyname ([^ ]+) (.+)/;
    this.commands['~rmdrugcategory'].regex = /^rmdrugcategory ([^ ]+) ([^ ]+)$/;
    this.commands['~rmdrugalias'].regex = /^rmdrugalias ([^ ]+) ([^ ]+)$/;
    this.commands['~setdrug'].regex = [/^setdrug ([^ ]+) ([^ ]+) (.+)$/, 4];
    this.commands['~adddrugsource'].regex = [/^adddrugsource ([^ ]+) ([^ ]+) (.+)$/, 4];
    this.commands['~rmdrugsource'].regex = [/^rmdrugsource ([^ ]+) ([^ ]+) (.+)$/, 4];
    this.commands['~setcat'].regex = [/^setcat ([^ ]+) ([^ ]+) (.+)$/, 4];
    this.commands['~staffstatus'].access = 'moderator';

    this.onLoad = function() {
/*        dbot.commands['~quiet'].access = function(event) {
            if(event.channel == '#tripsit' || event.channel == '#sanctuary' || event.channel == '#stims' || event.channel == '#opiates' || event.channel == '##meth') {
                return dbot.access.voice(event);
            } else if(event.channel == '##wat'){
                return dbot.access.op(event);
            } else {
                return dbot.access.power_user(event);
            }
        }.bind(this);        
/* dbot.commands['~unquiet'].access = function(event) {
            if(event.channel == '#tripsit' || event.channel == '#sanctuary' || event.channel == '#stims' || event.channel == '#opiates' || event.channel == '##meth') {
                return dbot.access.voice(event);
            } else if(event.channel == '##wat'){
                return dbot.access.op(event);
            } else {
                return dbot.access.power_user(event);
            }
        }.bind(this);      */ 


       dbot.api.event.addHook('new_notify', function(notify, user) {
          if(user == 'thanatos' && notify.type == 'quiet') {
            var curTime = new Date().getTime();
            if(curTime - dbot.db.lastAutoquiet.time > dbot.db.autoQuietHighscore) {
              event.reply('New autoquiet highscore! We went ' + moment(curTime).from(dbot.db.lastAutoquiet, true) + ', beating the previous record of ' + moment.duration(dbot.db.autoQuietHighscore) + '!');  
              dbot.db.autoQuietHighscore = curTime - dbot.db.lastAutoquiet.time;
            }
            dbot.db.lastAutoquiet = { 'time': curTime, 'lastDuration': curTime - dbot.db.lastAutoquiet.time, 'quietee': notify.target, 'channel': notify.channel };
          }
        });

        dbot.api.event.addHook('new_user', function(user) {
            if(_.has(dbot.instance.connections.tripsit.channels, '#drugs')) {
                setTimeout(function() {
                      dbot.api.nickserv.auth(user.server, user.currentNick, function(result, primary) {
                          if(result === false) {
                              var nicks = [];
                              async.each(this.tNotifies, function(id, done) {
                                  dbot.api.users.getUser(id, function(err, user) {
                                      if(user) {
                                          nicks.push(user.currentNick);
                                      }
                                      done(false);
                                  }); 
                              }, function() {
                                  var type = '\u00033tripsit\u000f';
                                  dbot.api.report.notifyUsers(user.server, nicks, 
                                  dbot.t('new_tripsit_user', { 'type': type, 'user': user.currentNick }));

                                  dbot.api.event.emit('new_tripsit_user', [ user ]);

                                  // Add a check
                                  dbot.api.users.resolveUser(user.server, dbot.config.name, function(err, bot) {
                                      /*setTimeout(function() {
                                        dbot.api.event.emit('new_tripsit_user_finished', [ user ]);
                                      }, 900000);*/
                                      this.tsChecks[user.id] = setTimeout(function() {
                                            //dbot.api.report.notify('tripsit', user.server, bot, 
                                            //    '#teamtripsit', 'No #tripsit notification observed for ' +
                                            //    user.currentNick);
                                          dbot.api.event.emit('tripsit_no_notify', [ user ]);
                                          delete this.tsChecks[user.id];
                                      }.bind(this), 600000);
                                  }.bind(this));
                              }.bind(this));
                          }
                      }.bind(this));
                }.bind(this), 10000);
            }
        }.bind(this));

        dbot.api.event.addHook('new_warning', function(warner, warnee, reason) {
        if(reason.match('#note')) {
          return;
        }
		var count = 0;
		var source_count = 0;
		dbot.modules.warning.db.search('warnings', { 'warnee': warnee.id }, function(warn) {
          if(!warn.reason.match('#note')) {
			count++;
			if(warn.reason.match('source') || warn.reason.match('sourcing')) {
				source_count++;
			}	
          }
		}, function() {
			if(source_count > 1) {
				dbot.say('tripsit', '#tripsit.me', warner.currentNick + ': ' +
					warnee.primaryNick + ' has been warned for sourcing ' + source_count + ' times. ' +
					' Maybe it\'s time for a holiday?');
			}
			if(count > 2) {
				dbot.say('tripsit', '#tripsit.me', warner.currentNick + ': ' +
					warnee.primaryNick + ' has been warned a total of ' + count + ' times. ' +
					' Maybe it\'s time for a holiday?');
			}
		});
	});

        dbot.api.event.addHook('new_notify', function(notify, creator) {
            _.each(_.keys(this.tsChecks), function(id) {
                dbot.api.users.getUser(id, function(err, user) {
			if(user) {
				if(notify.message.match(escapeRegExp(user.primaryNick)) != null || notify.message.match(escapeRegExp(user.currentNick)) != null) {
				    clearTimeout(this.tsChecks[id]);
				    delete this.tsChecks[id];
				}
			}
                }.bind(this));
            }.bind(this));

            if(notify.channel === '#tripsit') {
                var nick = notify.message.split(' ')[0];
                if(_.include(this.recentNotifies, nick) && notify.message.indexOf('#update') == -1) {
                    dbot.modules.report.db.del('notifies', notify.id, function(){});
                    dbot.say('tripsit', '#teamtripsit', dbot.t('deleted_dupe', {
                        'alias': nick,
                        'creator': creator
                    }));
                } else {
                    this.recentNotifies.push(nick);
                    setTimeout(function() {
                        this.recentNotifies = _.without(this.recentNotifies, nick);
                    }.bind(this), 1800000);
                }
            }

        }.bind(this));

        if(_.has(dbot.modules, 'web')) {
            dbot.api.web.addIndexLink('/factsheet', 'Factsheets');
        }
    }.bind(this);

    this.listener = function(event) {
        if(event.action == '322') {
            var split = event.params.split(' ');
            split[1] = split[1].replace(/(>|<)/g, '');
            this.channels.push([ split[2], split[1] ]);
            this.channels = _.sortBy(this.channels, function(el) { return parseInt(el[0]); });
        } 
    }.bind(this);
    this.on = ['322', 'PRIVMSG'];
};

exports.fetch = function(dbot) {
    return new tripsit(dbot); 
};
